# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import sys
import argparse
import csv
import numpy as np
import cv2
import matplotlib.pyplot as plt


def parse_arguments():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('src_img', help='path to source image')
    parser.add_argument('src_csv', help='path to source csv file')
    parser.add_argument('--reset', help='reset from backup', action='store_true')
    return parser.parse_args()

def read_detections(csv_path):
    detections = []
    entries = []
    with open(csv_path, 'rb') as fin:
        reader = csv.DictReader(fin, delimiter=',')
        for row in reader:
            for k, v in row.items():
                try:
                    row[k] = float(v)
                except:
                    pass
            obj_id = int(float(row['ObjectNumber']))
            x = float(row['AreaShape_Center_X'])
            y = float(row['AreaShape_Center_Y'])
            r = float(row['AreaShape_MaximumRadius'])
            group = row['group']
            if group == 'A':
                detections.append((x, y, r, obj_id))
                entries.append(row)
    return entries

def adjust_bounds(x, y, shape):
    x = max(x, 0)
    y = max(y, 0)

    x = min(x, shape[1])
    y = min(y, shape[0])

    return int(x), int(y)

def plot_detection(canvas, detection, tl, color=(0, 0, 255)):
    center = (int(detection[0] - tl[0]), int(detection[1] - tl[1]))
    radius = int(detection[2])

    cv2.circle(canvas,
               center,
               radius,
               color,
               1)
    return canvas

def circle_profile(img, x, y, r):
    mask = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)
    cv2.circle(mask, (int(x), int(y)), int(r), 255, 1)

    # canvas = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    # cv2.circle(canvas, (int(x), int(y)), int(r), (0, 255, 0), 1)
    # cv2.imshow("cv: mask", canvas)
    # c = cv2.waitKey(0)

    profile = img[mask > 0]
    return profile

def refit(img, detection):
    ## cut window around detection
    x, y, r = detection

    R = 2*r

    tl = adjust_bounds(x - R, y - R, img.shape)
    br = adjust_bounds(x + R, y + R, img.shape)

    window = img[tl[1]:br[1], tl[0]:br[0]]

    canvas = cv2.cvtColor(window, cv2.COLOR_GRAY2BGR);
    canvas = plot_detection(canvas, detection, tl)

    x_in_window = x - tl[0]
    y_in_window = y - tl[1]
    best_score, best_r = 10000, r
    scores = []
    stds = []
    for r_test in range(int(r), int(R)):
        profile = circle_profile(window, x_in_window, y_in_window, r_test)
        score = np.nanmedian(profile)
        scores.append(score)
        std = np.nanstd(profile)
        stds.append(std)

    # for cur_r in range(int(r), int(R), 10):
    #     canvas = plot_detection(canvas, (x, y, cur_r), tl, (0, 255, 0))

    darkest_i = np.argmin(scores)
    dists = np.abs(np.array(scores) - 30)

    best_dist = 1000
    for i in range(len(scores)):
        if i > darkest_i and dists[i] < best_dist:
            best_dist = dists[i]
            best_r = int(r) + i

    canvas = plot_detection(canvas, (x, y, best_r), tl, (255, 0, 0))
    canvas = plot_detection(canvas, detection, tl)
    # cv2.imshow("cv: detection", canvas)

    # c = cv2.waitKey(20)
    # if c == ord('q'):
    #     sys.exit(1)

    # plt.plot(np.gradient(scores), label='grad')
    # plt.plot(scores, label='scores')
    # plt.plot(stds, label='stds')
    # plt.plot(dists, label='dists')
    # plt.legend()
    # plt.show()

    return (x, y, best_r)

def main(args):
    src_dir, img_full_name = os.path.split(args.src_img)
    img_name = os.path.splitext(img_full_name)[0]
    csv_path = args.src_csv
    if not os.path.exists(csv_path):
        raise IOError("{} not found!".format(csv_path))
    csv_dir, csv_name = os.path.split(csv_path)
    backup_file = os.path.join(csv_dir, 'before_fix_{}'.format(csv_name))
    if args.reset:
        os.rename(backup_file, csv_path)

    if os.path.exists(backup_file):
        raise IOError("{} already exists!  To start from the backup file, run this with --reset".format(backup_file))

    entries = read_detections(csv_path)

    img = cv2.imread(args.src_img, 0)
    img_canvas = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

    img_median = np.median(img)
    # cv2.imshow("cv: img", img)
    print('img_median: {}'.format(img_median))

    for entry in entries:
        detection = (entry['AreaShape_Center_X'],
                     entry['AreaShape_Center_Y'],
                     entry['AreaShape_MaximumRadius'],
                     int(entry['ObjectNumber']))
        det = (detection[0], detection[1], detection[2])
        manually_added = detection[3] == -1
        if manually_added:
            new_detection = det
        else:
            new_detection = refit(img, det)

        entry['AreaShape_MaximumRadius'] = new_detection[2]
        plot_detection(img_canvas, det, (0, 0))
        plot_detection(img_canvas, new_detection, (0, 0), (0, 255, 0))

    os.rename(csv_path, backup_file)

    keys = ['ImageNumber', 'ObjectNumber', 'AreaShape_Center_X',
            'AreaShape_Center_Y', 'AreaShape_Eccentricity',
            'AreaShape_MajorAxisLength', 'AreaShape_MaximumRadius',
            'AreaShape_MinorAxisLength', 'Number_Object_Number',
            'group']

    with open(csv_path, 'wb') as fout:
        writer = csv.DictWriter(fout, fieldnames=keys, delimiter=',')
        writer.writeheader()
        for entry in entries:
            to_write = {key: entry[key] for key in keys}
            writer.writerow(to_write)
    # cv2.imshow("cv: img", img_canvas)
    # cv2.waitKey(0)
    # cv2.imwrite(os.path.join("/tmp/tuky", '{}.png'.format(img_name)), img_canvas)
    print('Objects refitted, try running vis.py again')
    return 0

if __name__ == '__main__':
    args = parse_arguments()
    sys.exit(main(args))
