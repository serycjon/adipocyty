#!/usr/bin/python
import numpy as np
from matplotlib.widgets import RectangleSelector
import matplotlib.pyplot as plt
import os
import sys
import csv


def recompute_sizes(objects):
    final_results = []
    for obj in objects:
        keep_object = (obj['group'] == 'A')
        fix_diameter = False

        if keep_object:
            max_radius = obj['AreaShape_MaximumRadius']
            # px to microns
            diameter_mum = max_radius * 2 * 0.919

            volume_pl = ((4.0 / 3.0) * np.pi * (diameter_mum / 2.0)**3) / 1000

            final_results.append([diameter_mum, volume_pl])

    final_results.sort(key=lambda x: x[0])

    return final_results

class GUI:
    def __init__(self, img_path, objects, out_path):
        self.print_help()
        disable_default_plt_bindings()
        self.out_path = out_path
        self.final_out_path = os.path.join(
            os.path.dirname(out_path),
            'final_' + os.path.basename(out_path))
        self.img = plt.imread(img_path)
        self.fig, self.ax = plt.subplots()
        self.keymap = {'quit': 'q',
                       'groupA': 'a',
                       'groupB': 'b',
                       'group?': '?',
                       'remove': 'x',
                       'insert_mode': 'i',
                       'add_drawn': 'a',
                       'toggle_hide': 'h',
                       'help': 'f1',
                       'group_lock': 'l',
                       'save': 'w'}

        self.groups_locked = False
        self.hidden = False
        self.remove_confirmed = False
        self.quit_confirmed = False
        self.insert_mode = False
        self.circle_to_insert = None

        plt.imshow(self.img, cmap='gray')

        self.objects = objects
        draw_cells(objects, 'circle')

        self.title = plt.title('')
        self.set_title('')

        rectprops = dict(facecolor='blue', edgecolor='black',
                         alpha=0.2, fill=True)
        self.ax = plt.gca()
        self.RS = RectangleSelector(self.ax,
                                    self.select_callback,
                                    rectprops=rectprops,
                                    useblit=True)
        insert_lineprops = dict(color='red')
        self.insert_RS = RectangleSelector(self.ax,
                                           self.insert_callback,
                                           drawtype='line',
                                           lineprops=insert_lineprops,
                                           useblit=True)
        self.insert_RS.set_active(False)
        self.insert_RS.set_visible(False)
        plt.connect('key_press_event', self.selector_callback)
        self.zoom_handler = zoom_factory(self.ax, 1.3)
        self.selection = []
        self.show()

    def selector_callback(self, event):
        remove_confirmed = False
        quit_confirmed = False

        key = event.key
        if key == self.keymap['quit']:
            if self.quit_confirmed:
                save_ok = False
                if self.save():
                    sys.exit(0)
            else:
                quit_confirmed = True
                print('Really quit? Press \'{}\' one more time to confirm'.
                      format(self.keymap['quit']))
        elif self.insert_mode and key == self.keymap['add_drawn']:
            self.add_drawn()
        elif key == self.keymap['insert_mode']:
            self.toggle_insert_mode()
        elif key == self.keymap['groupA']:
            for obj_id in self.selection:
                obj = self.objects[obj_id]
                obj['group'] = 'A'
                self.recolor_obj(obj)
            self.fig.canvas.draw()
        elif key == self.keymap['groupB']:
            for obj_id in self.selection:
                obj = self.objects[obj_id]
                obj['group'] = 'B'
                self.recolor_obj(obj)
            self.fig.canvas.draw()
        elif key == self.keymap['group?']:
            for obj_id in self.selection:
                obj = self.objects[obj_id]
                obj['group'] = '?'
                self.recolor_obj(obj)
            self.fig.canvas.draw()
        elif key == self.keymap['remove']:
            if self.remove_confirmed:
                for obj_id in self.selection:
                    obj = self.objects[obj_id]
                    obj['artist'].remove()
                self.objects = [self.objects[i]
                                for i in range(len(self.objects))
                                if i not in self.selection]
                del self.selection[:]
                self.fig.canvas.draw()
            else:
                remove_confirmed = True
                print(('Really remove {} objects? Press \'{}\' one more time' +
                      ' to confirm.').format(len(self.selection), self.keymap['remove']))
        elif key == self.keymap['group_lock']:
            self.groups_locked = not self.groups_locked
            print('zamceno: {}'.format(self.groups_locked))
            self.set_title('')
        elif key == self.keymap['toggle_hide']:
            self.toggle_hide()
        elif key == self.keymap['help']:
            self.print_help(False)
        elif key == self.keymap['save']:
            self.save()
        else:
            print('unknown key pressed: {}'.format(event.key))

        self.remove_confirmed = remove_confirmed
        self.quit_confirmed = quit_confirmed

    def toggle_insert_mode(self):
        self.insert_mode = not self.insert_mode
        if self.insert_mode:
            print('Insert mode ON. Toggle with \'{}\''.format(self.keymap['insert_mode']))

            self.RS.set_active(False)
            self.insert_RS.set_active(True)

            # cancel selection
            for obj_id in self.selection:
                obj = self.objects[obj_id]
                self.recolor_obj(obj)
            self.fig.canvas.draw()
        else:
            print('Insert mode OFF. Toggle with \'{}\''.format(self.keymap['insert_mode']))
            if self.circle_to_insert:
                self.circle_to_insert.remove()
                self.circle_to_insert = None
                self.fig.canvas.draw()

            self.RS.set_active(True)
            self.insert_RS.set_active(False)
        self.set_title('')

    def set_title(self, text):
        if self.insert_mode:
            text += '- insert mode -'
        else:
            text += '- review mode -'
        if self.groups_locked:
            text += ' (lock on)'
        self.title.set_text(text)
        self.fig.canvas.draw()

    def save(self):
        print('saving')
        keys = ['ImageNumber', 'ObjectNumber', 'AreaShape_Center_X',
                'AreaShape_Center_Y', 'AreaShape_Eccentricity',
                'AreaShape_MajorAxisLength', 'AreaShape_MaximumRadius',
                'AreaShape_MinorAxisLength', 'Number_Object_Number',
                'group']

        try:
            with open(self.out_path, 'wb') as fout:
                writer = csv.DictWriter(fout, fieldnames=keys, delimiter=',')
                writer.writeheader()
                for obj in self.objects:
                    to_write = {key: obj[key] for key in keys}
                    writer.writerow(to_write)

            ## write the final list
            with open(self.final_out_path, 'wb') as fout:
                writer = csv.DictWriter(fout, fieldnames=['Diameter um',
                                                          'Volume pl'],
                                        delimiter=',')
                writer.writeheader()
                final_results = recompute_sizes(self.objects)
                for cell in final_results:
                    to_write = {'Diameter um': cell[0],
                                'Volume pl': cell[1]}
                    writer.writerow(to_write)
            print('saved final results in {}'.format(self.final_out_path))

            return True
        except Exception, err:
            print('Cannot save. Make sure the output file is not opened in Excel')
            print(err)
            return False


    def add_drawn(self):
        if self.circle_to_insert is None:
            return
        circle = self.circle_to_insert
        object = {'ImageNumber': 0,
                  'ObjectNumber': -1,
                  'AreaShape_Center_X': circle.center[0],
                  'AreaShape_Center_Y': circle.center[1],
                  'AreaShape_Eccentricity': 0,
                  'AreaShape_MajorAxisLength': circle.radius,
                  'AreaShape_MinorAxisLength': circle.radius,
                  'AreaShape_MaximumRadius': circle.radius,
                  'Number_Object_Number': -1,
                  'group': 'A'}
        draw_cells([object], mode='circle')

        self.objects.append(object)
        self.circle_to_insert.remove()
        self.circle_to_insert = None
        self.fig.canvas.draw()

    def toggle_hide(self):
        self.hidden = not self.hidden
        for obj in self.objects:
            circle = obj['artist']
            circle.set(visible=not self.hidden)
        self.fig.canvas.draw()

    def recolor_obj(self, obj):
        circle = obj['artist']
        circle.set(edgecolor=get_object_color(obj)['edge'],
                   facecolor=get_object_color(obj)['face'])

    def select_callback(self, event_click, event_release):
        corner1 = event_click.xdata, event_click.ydata
        corner2 = event_release.xdata, event_release.ydata
        # button = event_click.button

        rect = (corner1, corner2)
        for obj_id in self.selection:
            obj = self.objects[obj_id]
            self.recolor_obj(obj)

        self.selection = []
        for obj_id, obj in enumerate(self.objects):
            if self.groups_locked and obj['group'] != '?':
                continue

            circle = obj['artist']
            if is_inside(circle.center, rect):
                # print('{} - center @ {}'.format(circle, circle.center))
                circle.set(facecolor='y')
                self.selection.append(obj_id)
        self.fig.canvas.draw()
        # cancel removing of selection when the selection changes
        self.remove_confirmed = False
        self.quit_confirmed = False

    def insert_callback(self, event_click, event_release):
        ax = plt.gca()
        pt_1 = event_click.xdata, event_click.ydata
        pt_2 = event_release.xdata, event_release.ydata

        center = ((pt_1[0] + pt_2[0]) / 2,
                  (pt_1[1] + pt_2[1]) / 2)
        radius = np.sqrt((pt_1[0] - pt_2[0])**2 +
                         (pt_1[1] - pt_2[1])**2) / 2.0

        if self.circle_to_insert:
            self.circle_to_insert.remove()
            self.circle_to_insert = None

        self.circle_to_insert = plt.Circle(center, radius, alpha=0.5, facecolor='y')

        ax.add_artist(self.circle_to_insert)
        self.fig.canvas.draw()


    def show(self):
        plt.show()

    def print_help(self, full=True):
        if full:
            help_str = '''
------------------
NAPOVEDA
------------------
Mysi se da vybrat obdelnik, tim vyberes vsechna kolecka, ktere maji
stred v obdelniku. To, co je zrovna vybrane je vybarveno zlute. Vyberu
muzes priradit skupinu a/b/? stisknutim prislusne klavesy (? znamena
ze rusis prirazeni ke skupine).

Dal muzes kdykoliv stiskem 'h' (hide) skryt/zobrazit vsechna kolecka,
aby bylo lepe videt na fotku.  Dalsi vec, co by se mohla hodit je
zamceni zpracovanych bunek - klavesa 'l' (lock). Kdyz je zamek
zapnuty, tak se pri vyberu obdelnikem nevyberou kolecka, ktera uz maji
skupinu A, nebo B, takze si to tim neprepises.

Ulozis stiskem 'w' (write), ukoncis stiskem 'q' (quit) (automaticky se ulozi).

Odstraneni a pridavani bunek: Oznacene bunky lze (uplne) odstranit
stiskem 'x' (eXterminate! :D). Aby sis neco neumazala omylem, musi se
'x' zmacknout jeste jednou na potvrzeni smazani.

Pridavani novych bunek se dela v 'insert modu' - mezi normalnim
('review') modem a 'insert' modem se prepina klavesou 'i' (insert). V
insert modu kliknutim a tazenim mysi oznacis prumer. Po pusteni
tlacitka se vykresli prislusny kruh, pokud vyhovuje, stisknutim 'a'
(add) ho pridas, jinak tam nezustane. Po dokonceni pridavani muzes jit
zpatky do 'review' modu pomoci stisku 'i'.

seznam klavesovych zkratek dostanes stisknutim F1
-----------------
'''
        else:
            keymap_str = '\n'.join(map(lambda (k, v): '{}: {}'.format(k, v),
                                       sorted(self.keymap.iteritems())))
            help_str = '''
---------
klavesove zkratky:
{}
'''.format(keymap_str)
        print(help_str)


def zoom_factory(ax, base_scale=2.):
    fig = ax.get_figure()  # get the figure of interest
    start_xlim = ax.get_xlim()
    start_ylim = ax.get_ylim()

    def zoom_fun(event):
        # get the current x and y limits
        cur_xlim = ax.get_xlim()
        cur_ylim = ax.get_ylim()
        xdata = event.xdata  # get event x location
        ydata = event.ydata  # get event y location
        if (not xdata) or (not ydata):
            return

        # Get distance from the cursor to the edge of the figure frame
        x_left = xdata - cur_xlim[0]
        x_right = cur_xlim[1] - xdata
        y_top = ydata - cur_ylim[0]
        y_bottom = cur_ylim[1] - ydata
        if event.button == 'up':
            # deal with zoom in
            scale_factor = 1/base_scale
        elif event.button == 'down':
            # deal with zoom out
            scale_factor = base_scale
        else:
            # deal with something that should never happen
            scale_factor = 1
            print event.button
        # set new limits
        new_xlim = [max(xdata - x_left*scale_factor, start_xlim[0]),
                    min(xdata + x_right*scale_factor, start_xlim[1])]
        ax.set_xlim(new_xlim)
        new_ylim = [min(ydata - y_top*scale_factor, start_ylim[0]),
                    max(ydata + y_bottom*scale_factor, start_ylim[1])]
        ax.set_ylim(new_ylim)
        fig.canvas.draw()  # force re-draw

    # attach the call back
    fig.canvas.mpl_connect('scroll_event', zoom_fun)

    # return the function
    return zoom_fun


def is_inside(xy, rect):
    return xy[0] >= rect[0][0] and xy[0] <= rect[1][0] and \
        xy[1] >= rect[0][1] and xy[1] <= rect[1][1]


def get_object_color(obj):
    color = dict()
    if obj['group'] == '?':
        color['face'] = 'C0'
        color['edge'] = 'y'
    elif obj['group'] == 'A':
        color['face'] = 'g'
        color['edge'] = 'g'
    elif obj['group'] == 'B':
        color['face'] = 'r'
        color['edge'] = 'r'
    else:
        color['face'] = 'm'
        color['edge'] = 'm'
    return color


def disable_default_plt_bindings():
    bindings = ['keymap.all_axes',
                'keymap.back',
                'keymap.forward',
                'keymap.fullscreen',
                'keymap.grid',
                'keymap.home',
                'keymap.pan',
                'keymap.save',
                'keymap.xscale',
                'keymap.yscale',
                'keymap.zoom']
    for binding in bindings:
        plt.rcParams[binding] = ''


def draw_cells(objects, mode='scatter'):
    if mode == 'scatter':
        centers = [[obj['AreaShape_Center_X'], obj['AreaShape_Center_Y']]
                   for obj in objects]
        centers = np.array(centers)
        plt.scatter(centers[:, 0], centers[:, 1])

        return None
    elif mode == 'circle':
        ax = plt.gca()
        drawn = []
        for obj in objects:
            color = get_object_color(obj)['edge']
            coords = (obj['AreaShape_Center_X'], obj['AreaShape_Center_Y'])
            radius = obj['AreaShape_MaximumRadius']
            circle = plt.Circle(coords, radius, alpha=0.5, edgecolor=color,
                                facecolor=get_object_color(obj)['face'])

            ax.add_artist(circle)
            drawn.append(circle)
            obj['artist'] = circle

        return drawn


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('wrong argument number...')
        print('usage: python vis.py path_to_image path_to_FilteredObject')
    else:
        im_path = sys.argv[1]
        detections_path = sys.argv[2]

        out_name = os.path.basename(detections_path)
        out_name = 'out_' + os.path.splitext(out_name)[0] + '.csv'
        out_path = os.path.join(os.path.dirname(detections_path), out_name)

        delimiter = '\t'

        if os.path.exists(out_path):
            detections_path = out_path
            delimiter = ','

        print('loading {}'.format(detections_path))
        with open(detections_path, 'rb') as detections_file:
            reader = csv.DictReader(detections_file, delimiter=delimiter)
            objects = []
            for row in reader:
                for k, v in row.items():
                    try:
                        row[k] = float(v)
                    except:
                        pass
                if 'group' not in row:
                    row['group'] = '?'
                # print(row)
                objects.append(row)

        print('loading {}'.format(im_path))

        gui = GUI(im_path, objects, out_path)
