Jak to cele spustit:

1) spustit prikazovou radku:
  - bud vlevo dole vyhledat a napsat cmd?
  - nebo win+r, napsat cmd a enter (win+r = tlacitko windows na klavesnici + r)

2) dostat se do kouzelne slozky ( :) ):
  - prikaz 'cd jmenoslozky' skoci do slozky, t.j. napises neco jako:
      cd Documents/školamagistr/diplomka/kouzla/src...

      windowsy jsou trochu divny, ale melo by fungovat doplnovani
      tabulatorem a to dost pomuze, takze pises neco jako:
      cd Doc tab škola tab tab dipl tab kouz tab src enter

      taky muzes pouzit sipku nahoru pro vypsani minuleho prikazu

  (- prikaz 'dir' ti vypise slozky a soubory tam, kde zrovna jsi (muze se hodit, kdyz si nepamatujes nazvy slozek))

3) spustit programek:
  - ujistis se, ze nemas v excelu otevreny vystupni soubor out...FilteredObjects.csv (jinak to pak nejde ulozit)
  - napises: python vis.py
  - mezernik
  - pretahnes do prikazove radky obrazek, cimz se ti tam nakopiruje cesta k nemu.
  - mezernik
  - pretahnes do prikazove radky prislusny textak FilteredObjects
  - enter

4) yaaay!

Jak si stahnout novou verzi:
1) vlezt normalne windowsackym pruzkumnikem do slozky s programkem
2) kliknout nekam pravym a vybrat neco jako 'git gui'
3) nahore v menu Remote/fetch/origin, nebo tak nejak :D
4) pak bude asi potrema jeste v menu Merge/Local Merge a pak kliknout Merge
5) kdyby byl konflikt a merge nefungovalo:
   - pokud jsi sama delala nejake zmeny, ktere nechces, aby zmizely z povrchu zemskeho, tak mi napis mejl
   - jinak muzes udelat reset:
     1) zavri git gui
     2) misto nej stejnym zpusobem otevri git bash
     3) napis: (pak enter)
	git reset --hard
     4) tim se vsechny zmeny u tebe smazou a kdyz se vratis do git gui, tak uz by merge mel fungovat

Jak s tim pracovat:
Mysi se da vybrat obdelnik, tim vyberes vsechna kolecka, ktere maji
stred v obdelniku. To, co je zrovna vybrane je vybarveno zlute. Vyberu
muzes priradit skupinu a/b/? stisknutim prislusne klavesy (? znamena
ze rusis prirazeni ke skupine).

Dal muzes kdykoliv stiskem 'h' (hide) skryt/zobrazit vsechna kolecka,
aby bylo lepe videt na fotku.  Dalsi vec, co by se mohla hodit je
zamceni zpracovanych bunek - klavesa 'l' (lock). Kdyz je zamek
zapnuty, tak se pri vyberu obdelnikem nevyberou kolecka, ktera uz maji
skupinu A, nebo B, takze si to tim neprepises.

Ulozis stiskem 'w' (write), ukoncis stiskem 'q' (quit) (automaticky se ulozi, musi se stisknout dvakrat - pro potvrzeni).

Odstraneni a pridavani bunek: Oznacene bunky lze (uplne) odstranit
stiskem 'x' (eXterminate! :D). Aby sis neco neumazala omylem, musi se
'x' zmacknout jeste jednou na potvrzeni smazani.

Pridavani novych bunek se dela v 'insert modu' - mezi normalnim
('review') modem a 'insert' modem se prepina klavesou 'i' (insert). V
insert modu kliknutim a tazenim mysi oznacis prumer. Po pusteni
tlacitka se vykresli prislusny kruh, pokud vyhovuje, stisknutim 'a'
(add) ho pridas, jinak tam nezustane. Po dokonceni pridavani muzes jit
zpatky do 'review' modu pomoci stisku 'i'.

Vysledky se ukladaji pomoci 'w' (write), nebo automaticky pred zavrenim pomoci
'q' (quit) do souboru out_nazevvstupnihosouborusdetekcema.csv. Ma
stejnou strukturu, jako vstup, jenom pridany sloupecek group. Nove vytvorene
body poznas podle ObjectNumber -1. (nevim, co vsechno za sloupecky pouzivas, ale
ted je spravne vytvoreny center_x, center_y, maximumRadius. Na max radius jsou
nastavene i major/minor axis length, excentricita 0.)

Pokud existuje vystupni soubor (tj uz jsi ten obrazek nekdy zacala delat a
zmeny ulozila), tak se ti automaticky nacte ulozeny stav (cool, no ne? :D ).

seznam klavesovych zkratek dostanes stisknutim F1
